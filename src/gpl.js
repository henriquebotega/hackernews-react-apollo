import { gql } from "@apollo/client";

const FRAG_CAR = gql`
    fragment FragCar on Car {
        id
        name
        color
    }
`;

export const CAR_QUERY = gql`
    ${FRAG_CAR}
    query getAllCars {
        getAll {
            ...FragCar
        }
    }
`;

export const CAR_BY_ID = gql`
    ${FRAG_CAR}
    query getCarByID($id: ID!) {
        getByID(id: $id) {
            ...FragCar
        }
    }
`;

export const CREATE_CAR_MUTATION = gql`
    ${FRAG_CAR}
    mutation newCar($name: String!, $color: String!) {
        insertCar(name: $name, color: $color) {
            ...FragCar
        }
    }
`;

export const UPDATE_CAR_MUTATION = gql`
    ${FRAG_CAR}
    mutation editCar($id: ID!, $name: String!, $color: String!) {
        updateCar(id: $id, name: $name, color: $color) {
            ...FragCar
        }
    }
`;

export const DELETE_CAR_MUTATION = gql`
    mutation delCar($id: ID!) {
        deleteCar(id: $id) {
            id
        }
    }
`;
