import { useQuery, useMutation, useLazyQuery } from "@apollo/client";
import { useState } from "react";
import {
    CREATE_CAR_MUTATION,
    DELETE_CAR_MUTATION,
    UPDATE_CAR_MUTATION,
    CAR_BY_ID,
    CAR_QUERY,
} from "./gpl";

function Item({ item, cancel, updateItem }) {
    const [removeCar] = useMutation(DELETE_CAR_MUTATION, {
        variables: { id: item.id },
        update: (cache, { data: { deleteCar } }) => {
            let { getAll } = cache.readQuery({ query: CAR_QUERY });

            const nGetAll = [...getAll].filter(
                (f) => f.id !== deleteCar.id.toString()
            );

            cache.writeQuery({
                query: CAR_QUERY,
                data: { getAll: [...nGetAll] },
            });
        },
    });

    return (
        <p key={item.id}>
            {item.name} - {item.color}
            <button onClick={() => updateItem(item)}>Edit</button>
            <button
                onClick={() => {
                    cancel();
                    removeCar(item.id);
                }}
            >
                Delete
            </button>
        </p>
    );
}

function App() {
    const [item, setItem] = useState({ id: 0, name: "", color: "" });
    const { loading, data, refetch } = useQuery(CAR_QUERY);

    const [getByID] = useLazyQuery(CAR_BY_ID, {
        fetchPolicy: "no-cache",
        onCompleted: (dataByID) => {
            setItem({
                ...dataByID.getByID,
            });
        },
    });

    const [createLink] = useMutation(CREATE_CAR_MUTATION, {
        variables: { name: item.name, color: item.color },
        update: (cache, { data: { insertCar } }) => {
            let { getAll } = cache.readQuery({ query: CAR_QUERY });

            cache.writeQuery({
                query: CAR_QUERY,
                data: { getAll: [...getAll, insertCar] },
            });
        },
    });

    const [updateLink] = useMutation(UPDATE_CAR_MUTATION, {
        variables: { id: item.id, name: item.name, color: item.color },
        update: (cache, { data: { updateCar } }) => {
            let { getAll } = cache.readQuery({ query: CAR_QUERY });

            const nGetAll = [...getAll].map((m) => {
                if (m.id === updateCar.id) {
                    m = updateCar;
                }

                return m;
            });

            cache.writeQuery({
                query: CAR_QUERY,
                data: { getAll: [...nGetAll] },
            });
        },
    });

    const updateItem = async (item) => {
        await getByID({ variables: { id: item.id } });
    };

    const cancel = () => {
        setItem({ id: 0, name: "", color: "" });
    };

    const salvar = async () => {
        if (item.name === "" || item.color === "") {
            console.log("Voce deve preencher os campos corretamente");
            return;
        }

        if (item.id === 0) {
            await createLink();
            setItem({ id: 0, name: "", color: "" });
        } else {
            await updateLink();
            setItem({ id: 0, name: "", color: "" });
        }
    };

    return (
        <div>
            {loading && <div>Loading...</div>}

            {!loading && (
                <>
                    <button onClick={() => refetch()}>Refresh</button>

                    <div>
                        <div>
                            <label>Name</label>

                            <input
                                name="name"
                                value={item.name}
                                onChange={(e) =>
                                    setItem({ ...item, name: e.target.value })
                                }
                            />
                        </div>
                        <div>
                            <label>Color</label>

                            <input
                                name="color"
                                value={item.color}
                                onChange={(e) =>
                                    setItem({ ...item, color: e.target.value })
                                }
                            />
                        </div>

                        <button onClick={() => salvar()}>Save</button>
                        <button onClick={() => cancel()}>Cancel</button>
                    </div>

                    {data && (
                        <>
                            {data.getAll.map((obj) => (
                                <Item
                                    key={obj.id}
                                    item={obj}
                                    cancel={cancel}
                                    updateItem={updateItem}
                                />
                            ))}
                        </>
                    )}
                </>
            )}
        </div>
    );
}

export default App;
